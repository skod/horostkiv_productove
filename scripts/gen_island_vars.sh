#bin/bash
#this script generate list of variable that must be imported to plc, which read and write variable from PRA0100

f="island_vars.xsy";

echo -n "enter read offset  "
read read_offset;

echo -n "enter write offset  "
read write_offset;

offset=$read_offset;

printf '%b\n' '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<VariablesExchangeFile>
	<fileHeader company="Schneider Automation" product="Unity Pro XL V7.0 - 120823C" dateTime="date_and_time#2014-8-13-11:46:42" content="Variable source file" DTDVersion="41"></fileHeader>
	<contentHeader name="Project" version="0.0.55" dateTime="date_and_time#2014-8-8-12:9:17"></contentHeader>
	<dataBlock>'>$f;

create_vars(){
while read line
do
  if [[ $line = *"$1"* ]]
    then
	IFS=" " read -r var typeName name topologicaddr   <<< "$line"
	name=${name##*=\"};
	name=${name%\"};
	sname=${name##*_};
        printf '%b' '		<variables name="'$name  '" typeName="'$2'" topologicalAddress="%'$3 $offset '"></variables>' >>$f;
	printf '%b\n'>>$f;
	(( offset++ ));
   fi
done <  "variables.xsy"
}

create_vars T_ANA_IN_GEN  INT MW

create_vars T_DIS_IN_GEN  INT MW

if [ "$offset" -gt "$write_offset" ]
then
    echo "offset overlaping!!!";
    echo "oops :("
    exit;
fi;

offset=$write_offset;

create_vars T_ANA_OUT_GEN  INT MW

create_vars T_DIS_OUT_GEN  INT MW

printf '%b' '	</dataBlock>
</VariablesExchangeFile>' >>$f;

echo "OK :)";