#bin/sh

f="preset_dump.xml";

echo>$f;
while read line
do

    if [[ $line = *".codes_low"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi
    if [[ $line = *".codes_high"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="10000" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".chann_low"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".chann_high"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="20" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".sens_low"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="4" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi
    if [[ $line = *".sens_high"* ]]
    then
	IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="20" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

   if [[ $line = *".filt"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".eng_low"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".eng_high"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".DIType"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".minCnt"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="1" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".maxCnt"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="10" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".inpTagMin"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".inpTagMax"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".sensMin"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="4" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".sensMax"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="20" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".demper"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="1000" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi



    if [[ $line = *".pv_max"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".kp"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="1" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".period"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="500" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".ti"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="10000" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".pv_max"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".sp_max"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


    if [[ $line = *".cnt_fast"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="1" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi
    if [[ $line = *".cnt_fast"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="10" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi









   if [[ $line = *".out_inf"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="0" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi
    if [[ $line = *".out_sup"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi

    if [[ $line = *".out_max"* ]]
    then
        IFS=" " read -r item ident checked fName  value  connected  <<< "$line"
        printf '%b'  '  '$item' ' $ident' ' $checked' ' $fName ' value="100" '  $connected >>$f;
        printf '%b\n'>>$f;
        continue;
    fi


        printf '%b\n'  "  $line" >>$f;

done <  "Dump.xml"