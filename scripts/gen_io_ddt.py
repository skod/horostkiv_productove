#!/usr/bin/python
import glob
import os

class block:
    def __init__(self,name,type_,addr, number):
        self.name = name
        self.type_ = type_
        self.addr = addr
        self.number = number

    def b_addr(self):
	print self.addr

    def b_name(self):
	return self.name

    def b_type(self):
	return self.type_

    def b_number(self):
	return self.number

def get_name(line):
    para = line.split(",")
    return para[1].strip()

def get_rack_addr(line):
    para = line.rsplit(",", 1)
    return para[1].strip()

def gen_io():
    path = '../cm/'
    typeName="NO_TYPE_FOUND"
    lines = []
    file = open("io_ddt_.xsy", "w")
    header ="""<VariablesExchangeFile>
	<dataBlock>"""
    file.write( header+"\n")    

    for infile in glob.glob( os.path.join(path, 'hor_*_*.txt') ):
	print infile
	if infile.find("ai") != -1:
    	    typeName="REF_TO INT"
    	    rw_type = "1" 
        elif infile.find("ao") != -1:
    	    typeName="REF_TO INT"
    	    rw_type = "2"
	elif infile.find("di") != -1:
            typeName="REF_TO EBOOL"
            rw_type = "1"
	elif infile.find("do") != -1:
            typeName="REF_TO EBOOL"
	    rw_type = "2"

	with open(infile) as f:
	    #line = f.readline()
	    for line in f:
    		file.write("<variables typeName=\"" + typeName+"\"" + " name=\""+get_name(line) +"\">"+"\n")
    		file.write("<attribute name=\"RWReferencedTypeRight\" value=\""+rw_type+"\"></attribute>"+"\n")
    		file.write("<variableInit value=\"REF(" + get_rack_addr(line) + ")\"/>"+"\n")
    		file.write("</variables>"+"\n")
    	f.close()

    filler ="""        </dataBlock>
</VariablesExchangeFile>"""

    file.write(filler+"\n")    
	
gen_io()