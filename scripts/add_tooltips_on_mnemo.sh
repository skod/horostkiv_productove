#!/bin/bash
mnemo="mnemo.xml";
signals="signals.txt"
output="mnemo_with_tooltips.xml"
cat /dev/null > $output
n=0;
k=0;
read_en=0;
linenum=0;
wasfound=0;
ao_cnt[1]='Короткий імпульс на відкриття';
ao_cnt[2]='Короткий імпульс на закриття';
ao_cnt[3]='Довгий імпульс на відкриття';
ao_cnt[4]='Довгий імпульс на закриття';
ao_cnt[5]='Переключення в автоматичний режим';
ao_cnt[6]='Переключення в ручний режим';
do_cnt[0]='Виключити';
do_cnt[1]='Включити';


Find_sourcer(){
found_comment='';
found_sig_type='None';
			while read signal
			do
			
						
			if [[ $signal = "" ]]
				then
				read_en=0;
				sig_type=None;
			fi
			if [[ read_en -eq 1 ]]
				then
				IFS="," read -r  num name comment other   <<< "$signal"
				IFS=" " read -r  name other   <<< "$name"
				if [[ $1  == *$name* ]]
					then
					found_comment=$comment;
					found_sig_type=$sig_type;
					read_en=0;
					name='';
					num=0;
					comment='';
					break;
				fi
			fi
			if [[ $signal = *"[AI]"* ]]
				then
				read_en=1;
				sig_type=AI;
			fi
			if [[ $signal = *"[AO]"* ]]
				then
				read_en=1;
				sig_type=AO;
			fi
			if [[ $signal = *"[DI]"* ]]
				then
				read_en=1;
				sig_type=DI;
			fi
			if [[ $signal = *"[DO]"* ]]
				then
				read_en=1;
				sig_type=DO;
			fi
			done <  $2;
			

}

while read line
do
(( linenum++ ))
	if [[ $line = *"<p n=\"SRC\">"* ]]
        then
		n=1;
	fi

	if [[ $n == 0 ]] 
		then	
		printf 	'%b' "$line
" >> $output;
	fi
	
	if [[ $read_cmd == 1 ]] 
		then
		if [[ $line = *"<float>"* ]]
			then
			IFS=">" read -r  cmd1 cmd2 cmd3   <<< "$line"
			cmd=${cmd2%*<*};
		fi
	fi
	read_cmd=0;
	if [[ $line = "<p n=\"CMD\">" ]]
		then
		read_cmd=1;
	fi
		
	if [[ $n == 1 ]] 
		then
		if [[ $line = *"<QString>"* ]]
			then
			IFS=" " read -r  src   <<< "$line"
			src=${src##*<QString>};
			src=${src%*</QString>};	
			child=${src##*.};
			Find_sourcer $src $signals;
			
			if [[ $child == "cnt" ]]
				then
				if [[ $found_sig_type = "AO" ]]
					then
					found_comment=${ao_cnt[$cmd]};
				fi
				if [[ $found_sig_type = "DO" ]]
					then
					found_comment=${do_cnt[$cmd]};
				fi
			fi
		echo $linenum $found_sig_type $src $child $found_comment;
		fi
		
		if [[ $k == 1 ]]
			then
			printf 	'%b' '<p n="SRC">
<QString>'$src'</QString>
</p>
' >> $output;

			if [[ $line = *"<p n=\"TLTP\">"* ]]
			then
				printf 	'%b' "$line
" >> $output;
			else
				printf  '%b' "<p n=\"TLTP\">\n<QString>$found_comment</QString>\n</p>
$line
" >> $output;
			fi
		n=0;
		k=0;
		fi
		if [[ $line = *"</p>"* ]]
			then
			k=1;
		fi
	fi
	#if [[ $n == 1 ]] 
#		then
#		if [[ $line = *"</p>"* ]]
#			then
			#sed -e "s|<QString>$src</QString>\n</p>|<\/p>\n<p n=\"TLTP\">\n<QString>$found_comment<\/QString>\n<\/p>|" $mnemo > mnemo_with_tooltips.xml;
			#sed -e "$linenum a <p n=\"TLTP\">\n<QString>$found_comment<\/QString>\n<\/p>" $mnemo > mnemo_with_tooltips.xml;
#			printf  '%b' "<p n=\"TLTP\">\n<QString>$found_comment</QString>\n</p>
#" >> $output;
#			n=0;
#		fi
#	fi

done <  $mnemo
printf 	'%b' "$line" >> $output;

echo "end";