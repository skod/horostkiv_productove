#bin/bash

f="hor_aparat.txt"

counter=0
while read line
do

    IFS="," read -r number translit_var_name db<<< "$line"
    

	if [ "$line" = "[AI]" ] || [ "$line" = "[DI]" ] || [ "$line" = "[AO]" ] || [ "$line" = "[DO]" ] || [ "$line" = "[OTHER]" ] ||
	   [ "$line" = "[PROJ]" ] || [ "$line" = "" ] || [ "$line" = "[LOOPS]" ] || [ "$line" = "[LATCH2DI2DO]" ] || [ "$line" = "[LATCH1DI2DO]" ] ||
	   [ "$line" = "[LATCH1DI1DO]" ] || [ "$line" = "[PULSE_COUNTER]" ] || [ "$line" = "[TABLE]" ] || [ "$line" = "[DV]" ] || [ "$line" = "[APARAT]" ] ||
	   [ "$line" = "[BLOCK]" ] || [ "$line" = "[L_BLOCK]" ] || [ "$line" = "[IVAR]" ] || [ "$line" = "[FVAR]" ]
        then
	    printf '%b\n' "$line" >> $f;
            continue;
	fi;

        if [ ${#translit_var_name}  -gt 24 ];
	then
            echo "too long[${#translit_var_name}] " $line
            (( counter++ ))
	fi;

	printf '%s' "$number" >> $f;
	printf '%b' "," >>$f;
	printf '%b' "$translit_var_name" >>$f;
	printf '%*s' $((${#translit_var_name} - 32)) "" >>$f;
	printf '%b' "," >>$f;
	printf '%b' "$name"  >> $f;
	printf '%b' "$db">>$f;
	printf '%b\n' "">>$f;

done < "c1.txt"

echo "counter " $counter 

printf '%b' $file >>$f;

